#ifndef _SUBLIB_2_H_
#define _SUBLIB_2_H_

#include <iostream>

class sublib2
{
public:
    void print()
    {
        std::cout << "Hello header only sub-library 2!" << std::endl;
    }    
};

#endif  // _SUBLIB_2_H_