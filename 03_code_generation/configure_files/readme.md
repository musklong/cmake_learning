# Introduction

During the call to cmake it is possible to create files that use variables from
the CMakeLists.txt and cmake cache. During CMake generation the file is copied to a new location and any cmake variables are replaced.
