# Introduction

This example shows how to generate source files using https://github.com/google/protobuf[protobuf].
Protocol Buffers is a data serialization format from Google. A user provides a
`.proto` file with a description of the data. Then using the protobuf compiler, the proto file
can be translated into source code in a number of languages including C++.
