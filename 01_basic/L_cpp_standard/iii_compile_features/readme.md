# Introduction

This example shows how to set the C++ standard using the `target_compile_features` function. This is available since CMake v3.1