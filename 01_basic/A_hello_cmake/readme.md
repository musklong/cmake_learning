# Introduction
Show a very basic hello world example.
The files in this tutorial are below:
~~~
A_hello_cmake$truee
.
├── CMakeLists.txt
├── main.cpp
└── readme.md
~~~

# Building the examples
~~~
$ mkdir build
$ cd build
$ cmake ..
$ make
$ ./hello_cmake
Hello CMake!
~~~