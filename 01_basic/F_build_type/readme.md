# Introduction

CMake has a number of built in build configurations which can be used to compile
your project. These specify the optimization levels and if debug information is
to be included in the binary.

The levels provided are:

  * Release - Adds the `-O3 -DNDEBUG` flags to the compiler
  * Debug - Adds the `-g` flag
  * MinSizeRel - Adds `-Os -DNDEBUG`
  * RelWithDebInfo - Adds `-O2 -g -DNDEBUG` flags

The files in this tutorial are below:

```
$ tree
.
├── CMakeLists.txt
├── main.cpp
```

利用cmake gui配置编译版本

Debug: 调试版本
Release: 正式版本
RelWithDebInfo: 既优化又能调试的版本
MinSizeRel: 最小体积版本