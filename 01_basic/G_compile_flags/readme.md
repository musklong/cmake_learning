# Introduction

CMake supports setting compile flags in a number of different ways:

  * using +target_compile_definitions()+ function
  * using the +CMAKE_C_FLAGS+ and +CMAKE_CXX_FLAGS+ variables.

The files in this tutorial are below:

```
$ tree
.
├── CMakeLists.txt
├── main.cpp
```