# Set the minimum version of CMake that can be used
# To find the cmake version run
# $ cmake --version
cmake_minimum_required(VERSION 3.5)

# Set the project name
get_filename_component(CWD ${CMAKE_CURRENT_LIST_DIR} NAME)
project (${CWD})

# Add an executable
add_executable(${CWD} main.cpp)
