cmake_minimum_required(VERSION 3.5)

get_filename_component(CWD ${CMAKE_CURRENT_LIST_DIR} NAME)
project(${CWD})

############################################################
# Create a library
############################################################

# Generate the static library from the library sources
add_library(${CWD}_library STATIC
    src/Hello.cpp
)
target_include_directories(${CWD}_library
    PUBLIC
        ${PROJECT_SOURCE_DIR}/include/static
)

############################################################
# Create an executable
############################################################
# Add an executable with the above sources
add_executable(${CWD} 
    src/main.cpp
)

# link the new hello_library target with the hello_binary target
target_link_libraries(${CWD}
    PRIVATE 
        ${CWD}_library
)
