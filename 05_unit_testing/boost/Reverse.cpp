#ifndef _REVERSE_H_
#define _REVERSE_H_

#include <string>

/**
 * Trivial class whose only function is to reverse a string.
 * Should use std::reverse instead but want to have example with own class
 */
class Reverse
{
public:
    std::string reverse(std::string &toReverse);
};
#endif // _REVERSE_H_