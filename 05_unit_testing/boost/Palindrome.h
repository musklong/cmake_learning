#ifndef _PALINDROME_H_
#define _PALINDROME_H_

#include <string>

/**
 * Trivial class to check if a string is a palindrome 
 */
class Palindrome
{
public:
    bool isPalindrome(const std::string &toCheck);
};

#endif // _PALINDROME_H_